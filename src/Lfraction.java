import java.util.*;
 
 /** This class represents fractions of form n/d where n and d are long integer 
  * numbers. Basic operations and arithmetics for fractions are provided.
  */
 public class Lfraction implements Comparable<Lfraction> {
 
    /** Main method. Different tests. */
    public static void main (String[] param) {
       // TODO!!! Your debugging tests here
	   //http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
    }
 
    /** numerator */
    private long numerator = 0;
 
    /** denominator > 0 */
    private long denominator = 1;
    // TODO!!! instance variables here
 
    /** Constructor.
     * @param a numerator
     * @param b denominator > 0
     */
    
    public Lfraction () {
    	throw new ArithmeticException (" Denominator and Numerator are missing ");
  }

    
    public Lfraction (long a) {
	          throw new ArithmeticException (" Denominator is missing ");
  }

    public Lfraction (long a, long b) {
 	      if (b > 0) {
 	          numerator = a;
 	          denominator = b;
 	       } else if (b < 0) {
 	          numerator = -a;
 	          denominator = -b;
 	       } else 
 	          throw new ArithmeticException (" illegal denominator zero ");
    }
 
    /** Public method to access the numerator field. 
     * @return numerator
     */
    public long getNumerator() {
       return numerator; // TODO!!!
    }
 
    /** Public method to access the denominator field. 
     * @return denominator
     */
    public long getDenominator() { 
       return denominator; // TODO!!!
    }
 
    /** Conversion to string.
     * @return string representation of the fraction
     */
    @Override
    public String toString() {
 	     return String.valueOf (getNumerator()) + "/" 
 	             + String.valueOf (getDenominator()); // TODO!!!
    }
 
    /** Equality test.
     * @param m second fraction
     * @return true if fractions this and m are equal
     */
    @Override
    public boolean equals (Object m) {
 	   return (this.compareTo((Lfraction) m) == 0);
    }
 
    /** Hashcode has to be equal for equal fractions.
     * @return hashcode
     */
    @Override
    public int hashCode() {
 	   return (int) (Double.doubleToLongBits (toDouble())>>31); // TODO!!!
    }
 
    /** Sum of fractions.
     * @param m second addend
     * @return this+m
     */
    public Lfraction plus (Lfraction m) {
    return new Lfraction (numerator*m.denominator + denominator*m.numerator, 
    denominator*m.denominator).reduce(); // TODO!!!
    }
 
    /** Multiplication of fractions.
     * @param m second factor
     * @return this*m
     */
    public Lfraction times (Lfraction m) {
 	   return new Lfraction (numerator*m.numerator, denominator*m.denominator)
 		         .reduce(); // TODO!!!
    }
 
    /** Inverse of the fraction. n/d becomes d/n.
     * @return inverse of this fraction: 1/this
     */
    public Lfraction inverse() {
    	if (denominator == 0) {
    	throw new ArithmeticException (" illegal denominator zero ");
    	}else{ 
 	    return new Lfraction (denominator, numerator).reduce();
    	}// TODO!!!
    }
 
    /** Opposite of the fraction. n/d becomes -n/d.
     * @return opposite of this fraction: -this
     */
    public Lfraction opposite() {
 	   return new Lfraction (-numerator, denominator).reduce(); // TODO!!!
    }
 
    /** Difference of fractions.
     * @param m subtrahend
     * @return this-m
     */
    public Lfraction minus (Lfraction m) {
 	   return this.plus (m.opposite()); // TODO!!!
    }
 
    /** Quotient of fractions.
     * @param m divisor
     * @return this/m
     */
    public Lfraction divideBy (Lfraction m) {
    if (denominator == 0) {
    throw new ArithmeticException (" illegal denominator zero ");
    }else{ 
    return this.times (m.inverse());
    }// TODO!!!
    }
 
    /** Comparision of fractions.
     * @param m second fraction
     * @return -1 if this < m; 0 if this==m; 1 if this > m
     */
    @Override
    public int compareTo (Lfraction m) {
 	   if (m instanceof Lfraction) {
 	         Lfraction difference = this.minus (m).reduce();
 	         if (difference.numerator == 0)
 	            return 0;
 	         else if (difference.numerator > 0)
 	            return 1;
 	         else return -1;
 	      } else throw new ClassCastException ("cannot cast to Fraction");
 	   } // TODO!!!
 
    /** Clone of the fraction.
     * @return new fraction equal to this
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
 	   return new Lfraction (getNumerator(), getDenominator()); // TODO!!!
    }
    
    /** Reduce this fraction (and make denominator > 0).
     * @return reduced fraction
     */
  private static long gcd (long a, long b) {
	      long m = Math.max (Math.abs (a), Math.abs (b));
	      if (m == 0) throw new ArithmeticException (" zero in gcd ");
	      long n = Math.min (Math.abs (a), Math.abs (b));
	      while (n > 0) {
         a = m % n;
         m = n;
         n = a;
      }
	      return m;
	   }
   
    private Lfraction reduce() {
       Lfraction f = null;
       try {
          f = (Lfraction)clone();
       } catch (CloneNotSupportedException e) {};
       if (denominator == 0)
          throw new ArithmeticException (" illegal denominator zero ");
       if (denominator < 0) {
          f.numerator = -numerator;
          f.denominator = -denominator;
       }
       if (numerator == 0)
          f.denominator = 1;
       else {
          long n = gcd (numerator, denominator);
          f.numerator = numerator / n;
          f.denominator = denominator / n;
       }
       return f;
    }
 
    /** Integer part of the (improper) fraction. 
     * @return integer part of this fraction
     */
    public long integerPart() {
 	   Lfraction tmp = reduce();
 	      return tmp.getNumerator()/tmp.getDenominator(); // TODO!!!
    }
 
    /** Extract fraction part of the (improper) fraction
     * (a proper fraction without the integer part).
     * @return fraction part of this fraction
     */
    public Lfraction fractionPart() {
 	   return new Lfraction (numerator - denominator*integerPart(), denominator)
 		         .reduce(); // TODO!!!
    }
 
    /** Approximate value of the fraction.
     * @return numeric value of this fraction
     */
    public double toDouble() {
       return ((double)numerator) / ((double)denominator); // TODO!!!
    }
 
    /** Double value f presented as a fraction with denominator d > 0.
     * @param f real number
     * @param d positive denominator for the result
     * @return f as an approximate fraction of form n/d
     */
    public static Lfraction toLfraction (double f, long d) {
 	      if (d > 0)
 	          return new Lfraction ((long)(Math.round (f*d)), d);
 	       else
 	          throw new ArithmeticException (" illegal denominator "); // TODO!!!
    }
 
    /** Conversion from string to the fraction. Accepts strings of form
     * that is defined by the toString method.
     * @param s string form (as produced by toString) of the fraction
     * @return fraction represented by s
     */
    public static Lfraction valueOf (String s) {
 	      StringTokenizer st = new StringTokenizer (s, "/<>[](),");
 	      long num = 0;
 	      long den = 1;
 	      if (st.hasMoreTokens()) {
 	         num = Long.parseLong (st.nextToken().trim());
 	      } else {
 	         throw new IllegalArgumentException ("not fraction " + s);
 	      }
 	      if (st.hasMoreTokens()) {
 	         den = Long.parseLong (st.nextToken());
 	      } else {
 	         
 	         den = 1;
 	      }
 	      if (st.hasMoreTokens()) {
 	         throw new IllegalArgumentException ("not fraction " + s);
 	      }
 	      return new Lfraction (num, den);
 	   }
    }
//http://enos.itcollege.ee/~jpoial/java/naited/Fraction.java
 
 